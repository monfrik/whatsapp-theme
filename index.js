const body = document.querySelector('.web');
body.classList.add('monfrik');

document.title = "Васап, Д'он";

let countClicks = 0;

const intervalSearch = setInterval(() => {
  const search = document.querySelector('.monfrik .gQzdc ._2cLHw');
  if (search) {
    search.innerHTML = "Чё ищешь, бедолага?";
    clearInterval(intervalSearch);
  }
}, 500);

const intervalChat = setInterval(() => {
  const chats = document.querySelectorAll('._2wP_Y');
  if (chats && chats.length) {
    clearInterval(intervalChat);
    
    chats.forEach(chat => {
      chat.addEventListener('click', () => {
        const input = document.querySelector('.monfrik ._3F6QL._2WovP ._39LWd');
        if (input) {
          input.innerHTML = "Начирикать текст, Д'он";
        }
      })
    });
  }
}, 500);
